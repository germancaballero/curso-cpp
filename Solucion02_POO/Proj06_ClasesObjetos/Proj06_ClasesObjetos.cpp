
#include <iostream>
#include "Cabecera.h"
#include "Aperitivo.h"

void ejemplo_instanciacion();
void ejemplo_instanciacion_ptr();

int main()
{
    // ejemplo_instanciacion();
    ejemplo_instanciacion_ptr();
}

void ejemplo_instanciacion() {
    // Forma fácil: Instanciación guiada por el compilador

    Aperitivo aper_lunes;   // Igual que con una estructura, el compilador reserva la memoria. Por defecto usa el contructor por defecto
    // Con el punto (.) accedemos a las variables miembro y a sus métodos
    // pedir_aperitivo(&aper_lunes);
    // Ahora  objeto.metodo(params...);
    aper_lunes.mostrar();
    aper_lunes.pedir();
    aper_lunes.cambiarCantidad(30);
    aper_lunes.mostrar();
    
    Aperitivo aper_martes("Pincho moruno", 3, 7.5);
    // pedir_aperitivo(&aper_martes);
    // aper_martes.pedir();
    aper_martes.cambiarCantidad(2);
    aper_martes.cambiarPrecio(7);
    aper_martes.mostrar();

    // Prácticas:
    // 1) Modificar las funciones cambiar_cantidad y mostrar_aperitivo para que sean métodos de la clase Aperitivo
    // 2) Añadir propiedad precio y usarla allá donde sea necesaria (en los métodos anteriores)
    // 3) Crear un método calcular precio total que devuelva el total (multiplicar cantidad por precio), recogemos el valor y lo mostramos
}
// Vamos a instanciar dinámicamente objetos, y a destruirlos con new y delete

void ejemplo_instanciacion_ptr() {

    Aperitivo* aper_lunes;   
    // Necesitamos una dirección de memoria a un espacio reservado
    aper_lunes = (Aperitivo*) malloc(sizeof(Aperitivo));
    aper_lunes->mostrar();
    aper_lunes->pedir();
    aper_lunes->cambiarCantidad(30);
    aper_lunes->mostrar();

    // Mejor usar new para reservar memoria, sólo porque es más bonito, pero es idéntico a malloc().
    Aperitivo* aper_martes;
    aper_martes = new Aperitivo("Pincho moruno", 3, 7.5);
    aper_martes->cambiarCantidad(2);
    aper_martes->cambiarPrecio(7);
    aper_martes->mostrar();

    free(aper_lunes);
    delete aper_martes;
}