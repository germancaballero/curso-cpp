#include <iostream>
#include <fstream>  // Librería para funciones de ficheros

using namespace std;

int main()
{
    std::cout << "Hola mundo  ╩┤!\n";

    // Output File Stream: Flujo de fichero de salida
    std::ofstream fichero("mi_fich.txt");

    fichero << "Hola mundodisco!\n";
    fichero << "No lo he leido.\n";
    fichero.close();

    // Input File Stream: Flujo de fichero de entrada
    std::ifstream fich_entrada("mi_fich.txt");
    char linea1[128], linea2[128];
    fich_entrada.getline(linea1, 128);
    fich_entrada.getline(linea2, 128);

    cout << "LEIDO: " << linea1 << ", " << linea2 << "\n";

    char linea_codigo[1024];
    ifstream fich_codigo("Proj12_Ficheros.cpp");

    // EOF = End Of File. 
    // Mientra no sea el final del fichero fich_codigo, haz
    while (!fich_codigo.eof()) {
        fich_codigo >> linea_codigo;
        cout << linea_codigo << endl;
    }
    fich_codigo.close();
    unsigned char caracter;
    // uint32_t  caracter;
    fich_codigo.open("Proj12_Ficheros.cpp");
    while (!fich_codigo.eof()) {
        fich_codigo >> caracter;
        if (caracter < 32)
            cout << " [ " << (int) caracter << " ] " << endl;
        cout << caracter;
    }

    return 0;
}