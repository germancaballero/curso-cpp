#include <iostream> // Comentario
#include <iostream>
#include <fstream>  // Librería para funciones de ficheros

using namespace std;

// 1 - Leer un fichero de codigo fuente con fich.getline()
// 2 - Crear una cadena (string) con tooodo el fichero
// 3 - Mostrar la longitud (tamaño en) del fichero
// 4 - Contar cuantos puntos y coma tiene (string.at() ó [])
// 5 - Controlar mediante try/catch que existe el fichero
// 6 - Eliminar los comentarios, las lineas que empiezan por // acordaros de .erase()
// 7 - Guardar el mismo fichero fuente sin comentarios
string* leer_fichero(ifstream& fich) {
    char linea_codigo[1024];
    string *todo_fich = new string("");
    while (!fich.eof()) {
        fich.getline(linea_codigo, 1024);
        todo_fich->append(linea_codigo).append(
            !fich.eof() ? "\n" : "");
    }
    cout << "Tamanho: " << todo_fich->length() << endl;
    return todo_fich;
}
int buscar_puntos_comas(string* todo_fich) {
    int num_puntos_comas = 0;
    for (int i = 0; i < todo_fich->length(); i++) {
        if (todo_fich->at(i) == ';') {
            num_puntos_comas++;
        }
    }
    cout << "Numero de puntos y comas: " << num_puntos_comas << endl;
    return num_puntos_comas;
}
void quitar_comentarios(string* todo_fich) {

    for (int i = 0; i < todo_fich->length(); i++) {
        if (todo_fich->substr(i, 2) == "//") {
            cout << "Hay un comentario en " << i << endl;
            /* O buscamos manualmente con un bucle
            for (int j = i + 2;
                i < todo_fich->length() || todo_fich->at(j) != '\n';
                j++);   // { }*/
            // O usamos find
            int pos_return = todo_fich->find('\n', i);
            todo_fich->erase(i, pos_return - i);
        }
    }
    int pos_return; // Lo declaramos fuera del bucle

    do {
        pos_return = todo_fich->find("\n\n\n");
        if (pos_return != string::npos) {
            todo_fich->erase(pos_return, 1);
        }
    } while (pos_return != string::npos);
}

int main()
{
#if _WIN32
    std::cout << "_WIN32!\n";
#elif LINUX
    std::cout << "LINUX!\n";
#endif
    // Este comentario lo tiene que eliminar
    std::cout << "Hello World!\n";
    // Y este tambien
    string* todo_fich;
    ifstream fich_codigo;
    try {
        fich_codigo.open("Proj13_EjercicioFicheros.cpp");
        if (fich_codigo.fail()) {
            throw exception("Fallo No se ha podido abrir");
        } 
        else {
            todo_fich = leer_fichero(fich_codigo);
            buscar_puntos_comas(todo_fich);
            quitar_comentarios(todo_fich);
            cout << *todo_fich;
            delete todo_fich;
        }
    }
    catch (exception& ex) {
        cout << ex.what() << "\n";
    }    
}