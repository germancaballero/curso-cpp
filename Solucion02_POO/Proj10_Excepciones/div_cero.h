#pragma once
#include <iostream>
#include <cstdlib>
#include <exception>

using namespace std;

class div_cero : public exception
{
public:
   // div_cero() : exception("Error: division por cero...") { }
    
   // Sobreescribimos el m�todo what()
    const char* what() const throw()
    {
        return "Error: division por cero...";
    }
};

