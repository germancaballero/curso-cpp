// TIPOS DE ERRORES:
// Errores de sintaxis ó de compilación: El programa no es capaz de compilar(generar algún *.OBJ) por no entender algo que hayamos escrito mal
// Errores de linkado: El linkador no encuentra alguna referencia y/o no puede generar el *.EXE
// Excepciones: La excepciones es lo que llamamos casque, son errores que se lanzan en tiempo de ejecución por un comportamiento fallido (problemas con punteros, cálculos imposibles):
  //    Excepciones propias del lenguaje C++
  //    Excepciones diseñadas por el programador/a
// Errores lógicos: El programa no funciona como esperamos porque no se comporta como debería

#include <iostream>
#include <stdexcept>
#include "div_cero.h"
#include "ExcepcionTest.h"

using namespace std;

void provocar_excepcion(bool siProvocar) {
    if (siProvocar) {
        // Creamos un nuevo objeto de tipo Exception y lo lanzamos con throw
        // O lo creamos con alguna de las funciones ya definidas
        throw new invalid_argument("El argumento es inválido");
    }
    else {
        cout << "El argumento es false\n";
    }
}
void
recibe_referencia(int& x) { // En el fondo una referencia es una forma más cómoda de pasar punteros
    x *= 2;
    cout << "Se ha multiplicado por 2: " << x << endl;
}
void fun_provocar_stackorverflow(int z) {
    int x, y;
    if (z % 100 == 0)
        cout << "Entrando en fun() con z = " << z << "\n";
    if (z < 400) 
        fun_provocar_stackorverflow( ++z );    
    if (z % 100 == 0)
        cout << "Saliendo de la fun(), volviendo " << z << "\n" ;
}
void lanzar_excepcion_no_clase() {
    throw "Texto EXCEPCION cualquiera";
}
void funcion_sin_excepciones() noexcept {
    // Con esto evitamos la comprobación de excepciones, incrementa muy poco el rendimiento. 
    // Si ocurre una excepcion, el programa termina sin indicar qué ha pasado
    cout << "Sabemos que no provocaremos exepcion\n";
}
void funcion_excepcion_zero() {

    try {
        cout << "Escribe un numero para dividir: \n";
        int num;
        cin >> num;
        if (num == 0)
            throw div_cero();
        else
            cout << "Division: " << (10 / num) << endl;
    }
    catch (exception& ex) {
        cout << "ERROR GENERAL CAPTURADO: " << ex.what() << endl;
    }
    catch (div_cero& ex) {
        cout << "ERROR ZERO CAPTURADO: " << ex.what() << endl;
    }
}
int main()
{
    funcion_sin_excepciones();

    // Referencias: son como alias o etiquetas de una variable, pero en el fondo son punteros
    int n = 4;
    int& ref_n = n;
    // Lo que pasa es que se usan como variables (no hace falta usar la indirección, o símbolo de contenido ( *variable)
    cout << "Referencia a n = " << ref_n << endl;
    recibe_referencia(n);
    cout << "Se ha multiplicado por 2??? " << n << endl;

    try {
        lanzar_excepcion_no_clase();
    }
    catch (const char* str) {
        cout << "Excepcion formato texto: " << str << endl;
    }
    // Para controlar una excepción usamos el típico bloque try { ... } catch ( ... ) { ... }
    std ::cout << "Excepciones!\n";
    try {
        provocar_excepcion(true);
    }
    catch (invalid_argument* excepcion) {
        // excepcion es un objeto de tipo invalid_argument que hereda en última instancia, de la clase exception
        cerr << excepcion->what() << endl;   // endl == '\n'
        // Como ha habido un error, finalizamos el programa devolviendo un valor negativo
        // return -1;
    }
    // Si no terminamos el programa, como hemos capturado la excepción anterior, el programa continúa
    try {
        // Cuando se quiere capturar una excepción que no sabemos la clase, podemos usar la exption genérica
        throw ExcepcionTest("Mensaje excepcion propia", 99);
    }
    catch (exception& cualquierEx) {
        cerr << cualquierEx.what() << endl;   // endl == '\n'
    }
    catch (...) {
        cerr << "Alguna excepcion!" << endl;   // endl == '\n'
    }

    try {
        throw ExcepcionTest("Mensaje excepcion propia", 99);
    }
    catch (invalid_argument* excepcion) {
        // Aquí no entra porque no es de este tipo
    }
    catch (ExcepcionTest& ex) {
        cerr << ex.what() << " ERROR: " << ex.getCodError() << endl;   // endl == '\n'
    }
    catch (exception& cualquierEx) {
        // Pondremos la más genérica al final para que si encuentra las más explícitas, que coja la genérica
        cerr << cualquierEx.what() << endl;   // endl == '\n'
    }
    funcion_excepcion_zero();

    // STACK OVERFLOW no podemos capturarla. Como no hemos podido capturar la excepción, la dejamos al final
    fun_provocar_stackorverflow(0);
    return 0;
}
