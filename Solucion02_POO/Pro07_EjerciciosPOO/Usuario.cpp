#include "Usuario.h"

Usuario::Usuario(char nombre[]) {
	cout << "Creando usuario con nombre...\n";
	this->edad = 0;
	strcpy_s(this->nombre, 50, nombre);
}
Usuario::Usuario(const char* nombre) {
	cout << "Creando usuario con nombre...\n";
	this->edad = 0;
	strcpy_s(this->nombre, 50, nombre);
}
Usuario::Usuario(char nombre[], int edad) {
	cout << "Creando usuario con nombre y edad...\n";
	this->edad = edad;
	strcpy_s(this->nombre, 50, nombre);
}
Usuario::Usuario(const char* nombre, int edad) {
	cout << "Creando usuario con nombre y edad...\n";
	this->edad = edad;
	strcpy_s(this->nombre, 50, nombre);
}

void Usuario::pedirDatos() {
	cout << "\nIntro nombre: ";
	cin.getline(this->nombre, 50);
	cout << "\nIntro edad: ";
	cin >> this->edad;
}
