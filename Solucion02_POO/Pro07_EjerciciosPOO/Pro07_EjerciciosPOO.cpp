/* EJERCICIOS:
    1) Crear una clase Usuario
    2) Con 2 campos, edad y nombre
    3) Con un contructor por defecto, otro que reciba sólo el nombre (edad cero) y otro qe reciba ambos campos
    4) Un método para mostrar los datos del usuario
    5) Crear un nuevo usuario en su forma normal (NO puntero)
    6) Un método para pedir nuevos datos (edad y nombre)
    7) Un método cumpleaños que aumente en uno la edad
    8) Crear otro usuario pero tipo puntero, usando todos los métodos anteriores
    9) Crear un array de usuarios con los dos anteriores y alguno más
*/
#include "Cabecera.h"

int main()
{
    std::cout << "Ejercicio Usuarios\n";
    Usuario usu;    // El único que se liberará automáticamente es este por ser una variable normal
    usu.pedirDatos();
    usu.cumpleanhos();
    usu.cumpleanhos();
    //for (int i = 0; i < 2000000000; i++) {
    Usuario* ptr_usu = new Usuario("Pepito");
    ptr_usu->cumpleanhos();
    //}
    Usuario* ptr_usu2 = new Usuario("Grillo", 20);

    int bytes_array = sizeof(Usuario*) * 4;
    cout << bytes_array << " bytes para el array\n";
    Usuario** listaUsu = (Usuario**)malloc(bytes_array);
    ///cout << "listaUsu** = "
    // Equivalente al Usuario* listaUsu[];
    (listaUsu)[0] = &usu;
    listaUsu[1] = ptr_usu;
    listaUsu[2] = ptr_usu2;
    listaUsu[3] = new Usuario("Cuarto milenio Iker", 30);

    for (int i = 0; i < 4; i++) {
        listaUsu[i]->mostrarUsuario();
    }
    // Al ser un array de punteros, lo que reserva C++ de manera automática son los 32 bits (o 64) para guardar esos 4 punteros
    Usuario* listaUsu2[4];
    (listaUsu2)[0] = &usu;
    listaUsu2[1] = ptr_usu;
    listaUsu2[2] = ptr_usu2;
    listaUsu2[3] = new Usuario("Usuario diferente", 60);

    for (int i = 0; i < 4; i++) {
        listaUsu2[i]->mostrarUsuario();
    }
    // Directamente estamos instanciando los 4 usuarios, Así reservamos 224 bytes de memoria para nuevos usuarios, por eso se llama 4 veces al constructor por defecto
    cout << "¿Cuantos usuarios? ";
    int numUsu = 0;
    cin >> numUsu;
    // En C++ SÍ se pueden hacer arrays dinámicos directamente con new
    Usuario* nuevosUsu = new Usuario[numUsu];
    nuevosUsu[0].pedirDatos();
    nuevosUsu[0].mostrarUsuario();

    // ¡¡LIBERAMOS MEMORIA!!


    /*Usuario listaUsu[4];    // Así reservamos otros 224 bytes de memoria para nuevos usuarios, por eso se llama 4 veces al constructor por defecto
    listaUsu[0] = usu;
    listaUsu[1] = *ptr_usu;
    listaUsu[2] = *ptr_usu2;
    listaUsu[3].setEdad(20);
    listaUsu[3].setNombre("Fulano el del 4to");

    for (int i = 0; i < 4; i++) {
        listaUsu[i].mostrarUsuario();
    }*/
    // LA REGLA DE ORO: Por cada malloc() tiene que haber un free() y por cada new() un delete. Lo mejor es hacerlo a la vez
    delete ptr_usu;
    delete ptr_usu2;
    // Primero liberamos la memoria del contenido
    delete listaUsu[3];
    // Luego ya liberamos el array cuando todo su contenido está liberado
    free(listaUsu);

    delete listaUsu2[3];
    // Cuando liberamos un array, poner delete[] con corchetes
    delete[] nuevosUsu;
}

