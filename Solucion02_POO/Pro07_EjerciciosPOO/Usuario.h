#pragma once
#include "Cabecera.h"

class Usuario
{
// private: Por defecto son privadas
	int edad;
	char nombre[50];

public:
	// Cuando implementamos un m�todo en la cabecera, ese m�todo es un m�todo inline: En vez de llamar a una funci�n y luego volver al punto de partida (la siguiente l�nea de c�digo a la llamada), se incrusta el c�digo binario donde se llama: Es m�s r�pido (te ahorra una llamada) pero ocupa un poco m�s en .EXE
	Usuario() {
		cout << "Creando usuario por defecto...\n";
		this->edad = 0;
		// Ultra r�pido en rendimiento
		nombre[0] = '\0'; // strcpy_s(nombre, 50,  "");
	}
	// Sobrecarga de constructores: Hacer varios varios contructores con direrente estructura de par�metros
	Usuario(char nombre[]);
	Usuario(const char* nombre);
	Usuario(char nombre[], int edad);
	Usuario(const char* nombre, int edad);

	// M�todos de acceso (m�todos para leer y cambiar propiedaddes)
	// Getter y setters
	char* getNombre() {
		return this->nombre;
	}
	void setNombre(const char* nuevoNombre) {
		strcpy_s(this->nombre, 50, nuevoNombre);
	}
	int getEdad() {
		return this->edad;
	}
	void setEdad(int edad) {
		this->edad = edad;
	}


	// Los m�todos tambi�n se pueden sobrecargar y ser inline
	void mostrarUsuario() {
		this->mostrarUsuario("Usuario");	// Mejor llamamos a un m�todo de la clase en vez de copiar y pegar
		// cout << "Usuario " << this->nombre << " - edad " << this->edad << "\n";
	}
	void mostrarUsuario(const char *mensaje) {
		cout << mensaje << " " << this->nombre << " , edad " << this->edad << "\n";
	}
	void pedirDatos();
	void cumpleanhos() {
		this->edad++;
		cout << this->nombre << " ha cumplido " << this->edad << " anios\n";
	}
	// El destructor es el m�todo especial que se llama justo antes de liberar la memoria del objeto. Empieza por ~ (Alt Grf + 4 y SPC) seguido del nombre de la clase
	~Usuario() {
		// En los destructores hay que liberar la memoria de lo que hayamos reservado en esta clase
		cout << "Liberando memoria con free o delete " << nombre << "\n";
	}
};

