// Proj11_String.cpp : la clase string de C++ nos ayuda con los textos para facilitar su tratamiento, en vez de usar directamente arrays de char
#include <iostream>

#include <string>

using namespace std;

int main()
{
    std::string texto1("Hola que pasa");
    
    string texto2;
    cout << "Escribe algo pisha!" << endl;
    getline (cin, texto2);  // 
    std::cout << texto1 << endl;
    cout << texto2 << endl;

    // Interesante es que podamos comprarar cadenas:
    if (texto2 == texto1) {
        cout << "Te repites como los loros" << endl;
    }
    else {
        cout << "Anda, di Hola que pasa" << endl;
    }
    if (texto1 > texto2) {
        cout << "Tu texto es menor que Hola que pasa" << endl;
    } else {
        cout << "Tu texto es mayor o igual que Hola que pasa" << endl;
    }
    string todo_texto;
    todo_texto = "UNO - " + texto1 + " Y DOS - " + texto2 + "\n";
    cout << todo_texto;

    // Usar el string como  el array de char que es:
    cout << " Direccion memoria: " << &texto1 << endl;
    cout << " Contenido direcc memoria: " << *((char*) &texto1)
        << " - " << *(((char*)&texto1) + 1) << endl
        << " - " << *(((char*)&texto1) + 2) << endl
        << " - " << *(((char*)&texto1) + 3) << endl
        << " - " << *(((char*)&texto1) + 4) << endl
        << " - " << *(((char*)&texto1) + 5) << endl
        << " - " << *(((char*)&texto1) + 6) << endl
        << " - " << *(((char*)&texto1) + 7) << endl
        << " - " << *(((char*)&texto1) + 8) << endl
        << " - " << *(((char*)&texto1) + 9) << endl;
    cout << " Letra 0: " << texto1[0] << endl;
    cout << " Letra 3: " << texto1[3] << endl;
    cout << " Letra 6: " << texto1[6] << endl;
    cout << " Letra 6: " << texto1.at(6) << endl;
    cout << " Longituud 1: " << texto1.length() << endl;
    cout << " Longituud 2: " << texto2.length() << endl;
    if (texto2.empty()) 
        cout << "Texto 2 esta vaciko" << endl;
    else
        cout << "Texto 2 tiene texto" << endl;
    cout << " size 2: " << texto2.size() << endl;
    cout << " sizeof 2: " << sizeof(texto1) << endl;
    cout << " sizeof 2: " << sizeof(texto2) << endl;

    string texto_append = texto1.append(" texto añadido para siempre");
    cout << "Otra forma de concatenar: " << texto_append << endl;
    string texto_insert = texto1.insert(4, texto2);
    cout << "Texto insertado: " << texto_insert << endl;
    string texto_erase = texto1.erase(4, texto2.length() + 4);
    cout << "Texto borrado: " << texto_erase << endl;
}
