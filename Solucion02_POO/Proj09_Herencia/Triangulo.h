#pragma once
#include "Figura.h"
#include <math.h>
#include "InterfazImprimible.h"

// Triangulo, al heredar de la interfaz, debe implementar sus m�todos abstractos
class Triangulo : public Figura, public IImprimible
{
protected:
	float * lados;
public:
	// Un constructor puede depender de un constructor de la clase padre con ':'
	Triangulo(float l1, float l2, float l3) : Figura(3) {
		this->lados = NULL; // Con null indicamos que el puntero NO apunta a ning�n lado
		this->lados = new float[] {
			l1, l2, l3
		};
	}
	float* getLados() {
		return lados;
	}
	float perimetro() {
		if (this->lados != NULL) {
			return this->lados[0] + this->lados[1] + this->lados[2];
		}
		else {
			cout << "\nNo se puede calcular el per�metro del Triangulo\n";
			return 0;
		}
	}
	float area() {
		// semiperimetro
		float s = perimetro() / 2.0f;

		return sqrtf(s * (s - lados[0]) * (s - lados[1]) * (s - lados[2]));
	}
	void imprimir() {
		cout << "Triangulo " << perimetro() << " de perimetro. "
			<< area() << " de area\n";
	}
	~Triangulo() {
		delete[] this->lados;
	}
};

