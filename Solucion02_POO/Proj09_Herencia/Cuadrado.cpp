#include "Cuadrado.h"
#include <math.h>

float Cuadrado::area() {

	return  powf(lado, 2);	// Ser�a m�s eficiente lado * lado
}

void Cuadrado::imprimir() {
	cout << "Cuadrado " << perimetro() << " de perimetro. "
		<< area() << " de area\n";
}