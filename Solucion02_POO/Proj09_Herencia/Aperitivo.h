#pragma once
#include "InterfazImprimible.h"
#include <iostream>

using namespace std;
// Tres barras y sumary en formato xml es para generar documentaci�n de manera autom�tica con programas Doxygen (similar a javadoc)
/// <summary>
/// Clase es la estructura base, el molde o plantilla que van a tener las diferentes instancias (espacios de memoria reservados para guardar datos), o tambi�n llamados OBJETOS
/// </summary>
class Aperitivo : public IImprimible
{
private:	// Todas las propiedades deben ser privadas

	// Las propiedades mejor con sustantivos
	char nombre[50];
	int cantidad;
	float precio;
	// float total; Mejor no, porque lo adecuado es calcularlo cada vez que se pide

	float calcularTotal();
	// S�lo los m�todos que se usan fuera ser�n p�blicos
public:
	// Contructores son m�todos especiales para instanciar los objetos con unos valores de inicializaci�n
	// Al declararlos no devuelven un valor como tal, porque realmente devuelven una instancia. Su nombre debe coincidir exactamente como la clase
	Aperitivo();	// Contructor por defecto
	// La sobrecarga de m�todos (o de constructores) consiste en crear m�todos (o contructores) con el mismo nombre pero distinta esturctura de par�metros.
	Aperitivo(const char* nombre, int cantid, float prec);

	// En la cabecera s�lo declaramos los m�todos:
	// Los nombres de los m�todos, mejor verbos
	void pedir();
	void mostrar();
	void cambiarCantidad(int nuevaCantidad);
	void cambiarPrecio(float nuevoPrecio);
	void imprimir();
};

