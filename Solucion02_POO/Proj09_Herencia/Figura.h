#pragma once

#include <iostream>
using namespace std;

/// <summary>
/// Figura es una clase base, adem�s es abstracta porque tiene un m�todo abstracto. Tambi�n llamada clase padre (o madre)
/// </summary>
class Figura
{
// private:
protected:
	int numLados;	// As� podremos acceder directamente desde Cuadrado o Triangulo

public:
	Figura() { numLados = 0;  }

	Figura(int lados);

	int getNumLados() { return this->numLados; }

	// Al ser virtual, si se sobreescribe en una clase hija, un objeto con la forma de esta clase (la padre) llamar� al m�todo de la clase hija
	virtual float perimetro() {
		return 0;
	}
	// La manera de decir que toda figura tiene un m�todo PERO que no sabemos como implementarlo, es con 'abstract': Crear un m�todo abstracto o tambi�n llamado virtual puro: igualando a cero (NO HAY METODO a�n, s�lo en clases derivadas
	 virtual float area() = 0;
};

