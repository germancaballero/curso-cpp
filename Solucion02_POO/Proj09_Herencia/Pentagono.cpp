#include "Pentagono.h"

Pentagono::Pentagono(float* lados) 
	: Figura::Figura(5)	// No es obligatorio
{
	this->lados = (float* ) malloc(5 * sizeof(float));
	if (lados != NULL && this->lados != NULL) {
		for (int i = 0; i < 5; i++) {
			this->lados[i] = lados[i];
		}
	}
}
float Pentagono::perimetro() {
	float p = 0;
	int i = 0;
	do  {
		p += this->lados[i];
		i++;
	} while (i < 5);
	return p;
}

float Pentagono::area() {
	cout << "No se puede calcular as� de f�cil, ma�ana lanzaremos una excepci�n\n";
	//TODO: Lanzar excepci�n
	return  0;	
}
Pentagono::~Pentagono() {
	free(this->lados);
}