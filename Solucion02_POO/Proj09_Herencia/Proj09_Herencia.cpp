// Proj09_Herencia.cpp : 

#include <iostream>
#include "Triangulo.h"
#include "Cuadrado.h"
#include "Pentagono.h"
#include "Aperitivo.h"
using namespace std;

int main()
{
    std::cout << "Herencia!\n";

    /*
    Ya no podemos instaciar Figura porque es una clase abstracta
    Figura* genioYfig = new Figura(1);
    cout << "Lados figura: " << genioYfig->getNumLados()
        << ", perimetro: " << genioYfig->perimetro() << "\n";
        */
    Triangulo* tri = new Triangulo(3, 4, 5);
    cout << "Lados triangulo: " << tri->getNumLados() << "\n";

    // Podemos usar polimorfismo con herencia múltiple o interfaces
    IImprimible* tri_imp = tri;
    tri_imp->imprimir();

    Cuadrado * cuad = new Cuadrado(3.6);
    cout << "Cuadrado: Lados " << cuad->getNumLados()
        << ", long lado: " << cuad->getLado()
        << ", perimetro: " << cuad->perimetro() << "\n";

    
    // Por polimorfismo un objeto hijo puede obtener la forma del padre
    Figura* figCuad = cuad; // P.ej: un cuadrado puede ser figura

    cout << "Figura(Cuadrado): Lados " << figCuad->getNumLados()
        << ", perimetro: " << figCuad->perimetro()
       /* << " long lado: " << figCuad->getLado() */ << "\n";

    float lp[5] = {
        3, 4, 5, 6, 7
    };
    Pentagono* pent = new Pentagono(lp);

    cout << "Pentagono: Lados " << pent->getNumLados()
        << ", perimetro: " << pent->perimetro() << "\n";

    figCuad = pent; // Reusamos figCuad para PENTAGONO
    
    cout << "Figura(Pentagono): Lados " << figCuad->getNumLados()
        << ", perimetro: " << figCuad->perimetro() << "\n";

    // Como tenemos 4 figuras, vamos a crear un array de 5 pero añadiendo una figura más directamente
    // Recordamos que puntero a puntero suele ser (no tiene por qué) un puntero a un array.
    Figura** figuras = new Figura*[5];
    figuras[0] = NULL; // genioYfig; Ya no hay figura concreta
    figuras[1] = tri;
    figuras[2] = cuad;
    figuras[3] = pent;
    figuras[4] = new Cuadrado(3.2);
    for (int i = 0; i < 5; i++) {
        Figura* fig = figuras[i];
        //TODO: Capturar excepción en vez de if NULL
        if (fig != NULL) {
            cout << "Figura(" << i << "): Lados " << fig->getNumLados()
                << ", perimetro: " << fig->perimetro()
                << ", area: " << fig->area() << "\n";
        }
    }
    Aperitivo* ape = new Aperitivo("Gambas al ajillo", 3, 10.50f);
    
    IImprimible** tres_cosas = new IImprimible*[3];
    tres_cosas[0] = tri_imp;
    tres_cosas[1] = cuad;
    tres_cosas[2] = ape;
    cout << " *** IMPRIMIENDO ***\n";
    for (int i = 0; i < 3; i++)
        tres_cosas[i]->imprimir();
    cout << " *** FIN IMPRIMESION ***\n";

    for (int i = 0; i < 5; i++)
        if (figuras[i] != NULL)
            delete figuras[i];
    delete[] figuras;
    /* En vez de liberar uno por uno, usaremos el array
    delete genioYfig;    
    delete tri;
    delete cuad;
    delete pent;    */ 
}
/* Ejercicios:
  1 - Añadir propiedad 3 lados al triangulo, protected, con sus metodos getter
  2 - Implementar el método perimetro
  3 - Usarlo y mostrar valores ar y perim.
  4 - Crear una clase pentágono que herede de Figura,
  5 - y que tenga un array con 5 lados
  6 - Implementar (sobreescribir) perimetro, que los sume
  7 - Crear un pentagono con sus lados y demás y calcular el perimetro
  8 - Darle la forma de figura (como hicimos con figCuad)
  9 - Crear un array de punteros a figuras con todas las figuras, tri, cuad.. creadas
*/