#pragma once
#include "Figura.h"
class Pentagono : public Figura
{
private:
	float* lados;

public:
	Pentagono(float* lados);
		// : Figura()  Ponerlo o no es lo mismo, si no lo ponemos llama al consturcto por defecto
	float perimetro();
	float area();
	~Pentagono();
};

