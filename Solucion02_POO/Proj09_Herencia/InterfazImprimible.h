#pragma once

/* 
INTERFAZ: Es la forma de comunicarse una entidad con otra
- UI (User Interface) 
	es por donde el usuario interactura con el software
 - A.P.I. (Aplication Programming Interface):
	Lo que se publica de un programa para que otro programa lo use
 - Una interfaz en C++ es una clase puramente abstracta con m�todos abstractos. La forma (mensajes, m�todos) que tienen dos objetos de comunicarse.
 */
class IImprimible {
	// No tiene sentido hacer m�todos privados en una interfaz
	// No debe propiedades
public:
	// Lo que la indica la interfaz de cada m�todo es los tipos de los par�metros, el nombre del m�todo y el tipo que devuelve
	virtual void imprimir() = 0;
};