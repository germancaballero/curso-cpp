//En los ficheros Clase.cpp s�lo pondremos la definici�n (implementaci�n) de los m�todos
#include "Aperitivo.h"

// El constructor por defecto inicializa las variables con los valore spor defecto
Aperitivo::Aperitivo() {
    this->precio = 0;
    this->cantidad = 0;
    strcpy_s(this->nombre, "");
}
Aperitivo::Aperitivo(const char *nombre, int cantid, float prec) {
    this->precio = prec;
    this->cantidad = cantid;
    strcpy_s(this->nombre, nombre);
}
/*void pedir_aperitivo(Aperitivo* this) {

    cout << "Ingrese el aperitivo: ";
    cin.getline(this->nombre, 50);
    cout << "Cantidad aperitivo: ";
    cin >> this->cantidad;
}*/
// M�todos son funciones espef�ficas de una clase, donde SIEMPRE hay una variable de tipo puntero a Clase que funciona en forma de par�metro llamado siempre "this". Corresponde al objeto que llama a ese m�todo

//  tipo_devuelto  Clase::Metodo(param...) { ... }

void Aperitivo::pedir() {
    cout << "Ingrese el aperitivo: ";
    cin.getline(this->nombre, 50);
    cout << "Cantidad aperitivo: ";
    cin >> this->cantidad;
    cin.get();
    cout << "Precio : ";
    cin >> this->precio;
    cin.get();
}


void Aperitivo::mostrar() {
    cout << "Tienes " << this->cantidad << " de " << this->nombre << "\n"
        << "    Precio: " << this->precio << ", total: " << this->calcularTotal() << "\n";
}
// CUando un m�todo o propiedad tienen varias palabras, usamosLaNomenclatura camelCase
void Aperitivo::cambiarCantidad(int nuevaCantidad) {
    if (nuevaCantidad >= 0) {
        this->cantidad = nuevaCantidad;
        cout << "Nueva cantidad: " << nuevaCantidad << "\n";
    }
    else {
        cout << "Estas flipando, no te debo  " << nuevaCantidad << "\n";
    }
}

void Aperitivo::cambiarPrecio(float nuevoPrecio) {
    if (nuevoPrecio >= 0) {
        this->precio = nuevoPrecio;
        cout << "Nuevo precio: " << nuevoPrecio << "\n";
    }
    else {
        cout << "Estas flipando, no te debo  " << nuevoPrecio << "\n";
    }
}
float Aperitivo::calcularTotal() {
    float total = this->precio * this->cantidad;
    cout << "Total calculado:  " << total << "\n";
    return total;
}

void Aperitivo::imprimir() {
    this->mostrar();
}