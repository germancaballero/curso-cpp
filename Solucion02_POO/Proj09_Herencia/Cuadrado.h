#pragma once
#include "Figura.h"
#include "InterfazImprimible.h"

class Cuadrado : public Figura, public IImprimible
{
private: 
    float lado;

public:
    Cuadrado(float lado) : Figura(4) {
        this->lado = lado;
    }
    float getLado() { return this->lado; }

    // Estamos sobreescribiendo el m�todo del padre, por que tiene el mismo nombre y los mismos par�metros
    float perimetro() {
        return this->lado * this->numLados;
            // this->getLados(); Ya como es protected podemos usar la variable directamente
    }
    void imprimir();
    float area();
};

