#include <iostream>
// STL: Standar Template Library
#include <vector>
#include <string>
#include <map>
#include "Pareja.h"

using namespace std;

int main()
{
    // Un vector en C++ es un array dinámico de lo que queramos
    vector<int> mi_vector;  // No hace falta indicar el tamaño
    // 2, 4, 6, 8, y 10
    for (int i = 1; i <= 5; i++)
        mi_vector.push_back(i * 2); // Así añadimos elementos dinámicamente

    // Comprobamos si está vacio el vector:
    if (mi_vector.empty()) {
        cout << "\nVector esta vacio\n";
    }
    else {
        cout << "Tamanho: " << mi_vector.size() << "\n";
    }
    // ITERADORES: Un iterados es un objeto que guía, apuntador, puntero de por donde vamos recorriendo una coleccón
    // auto es el tipo de dato inferido (deducido) del valor.
    // P. ej un double se deduce de un número decimal
    auto dec = 3.454545f;

    //for (vector<int>::iterator it = mi_vector.begin();

    for (auto it = mi_vector.begin(); it != mi_vector.end(); ++it) {
        // El iterador  tiene sobrecargado el operador ++
        cout << *it << "\n"; // El contenido los mostramos con el operador de indirección, similiar al del contenido del puntero
    }
    // Insertar en vecor: 
    mi_vector.emplace(mi_vector.begin(), 40);
    // vector tiene también sobrecargado el operador de índice [ ]
    cout << mi_vector[0] << ", " << mi_vector[1] << "\n";
    // Limpiar el vector:
    mi_vector.clear();

    if (mi_vector.empty()) {
        cout << "\nVector esta vacio\n";
    }
    else {
        cout << "Tamanho: " << mi_vector.size() << "\n";
    }

    // Vector de string
    vector<string> textos;
    string texto_intro;
    do {
        char txt[100];
        cout << "\nEscribe algo: ";
        // cin >> texto_intro;
        cin.getline(txt, 100);
        texto_intro = txt;
        if (!texto_intro.empty())
            textos.push_back(texto_intro);
    } while (!texto_intro.empty());

    for (auto it = textos.begin(); it != textos.end(); ++it) {
        cout << *it << "\n";
    }
    textos.clear();

    map<string, Pareja<int>> dicc;
    dicc.insert(pair<string, Pareja<int>>(
        string("AAA"), Pareja<int>(4, 7)));
    dicc.insert(pair<string, Pareja<int>>(
        string("BBB"), Pareja<int>(6, 33)));
    dicc.insert(pair<string, Pareja<int>>(
        string("CCC"), Pareja<int>(7, 22)));
    dicc.insert(pair<string, Pareja<int>>(
        string("DDD"), Pareja<int>(9, 44)));

    Pareja<int> un_par = dicc.at("CCC");
    cout << "\nMAX PAR (22): " << un_par.GetMax();

    // ¿Para sumar parejas?
    Pareja<int> suma(0, 0);
    suma = dicc.at("AAA") << un_par; // (11, 29)
    // suma = dicc.at("AAA") + un_par; // (11, 29)
    cout << "\nMAX PAR SUMA (22): " << suma.GetMax();
    cout << "\nEl 2º: " << suma[1];

}
