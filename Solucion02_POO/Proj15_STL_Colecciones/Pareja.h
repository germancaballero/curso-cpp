#pragma once

template <class Tipo>
Tipo GetMaxT(Tipo a, Tipo b);

// Una clase que defina un array de 2 elementos (pareja) de alg�n tipo
// Esta clase a priori no se compila, s�lo se compila cada vez que la usamos con un nuevo tipo
// Como no se compila, todos la implementaci�n (de m�todos) tiene que estar en el heade (Pareja.h)
template <class T>
class Pareja {
    T valores[2];
public:
    Pareja(T primer, T segun) {
        valores[0] = primer;
        valores[1] = segun;
    }
    T GetMax() {
        // Incluso podemos reusar otros templates como la funci�n GetMaxT
        return valores[0] > valores[1] ? valores[0] : valores[1];
    }
    // Sobrecargar el operador
    Pareja<T> operator +(Pareja<T> p) {
        T v1 = valores[0] + p.valores[0];
        T v2 = valores[1] + p.valores[1];
        return Pareja<T>(v1, v2);
    }
    T operator[](int pos) {
        return this->valores[pos];
    }
    Pareja<T> operator << (Pareja<T> p) {
        T v1 = valores[0] + p.valores[0];
        T v2 = valores[1] + p.valores[1];
        return Pareja<T>(v1, v2);
    }
};
