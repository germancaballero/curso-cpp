#pragma once

#include "Animal.h"
#include <iostream>

using namespace std;

class OtraClaseDelPrograma
{
public:
	OtraClaseDelPrograma() {
		cout << "Por lo que sea, mostramos el peso total que es: \n" << 
			Animal::pesoTotal << "\n";
	}
};

