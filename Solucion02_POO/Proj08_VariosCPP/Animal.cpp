
#include "Animal.h"

// Inicializamos la variable estática SÓLO UNA VEZ en todo el programa
// La inicialización de las variables estáticas se ejecuta ANTES QUE el propio main()
int Animal::pesoTotal = 0;
int Animal::totalAnimales = 0;
int Animal::pesoMaximo = 0;
int Animal::pesoMinimo = INT_MAX;
