#pragma once
#include "Animal.h"

/* Eso es lo que se llama composici�n: Un objeto que se compone de otro
Un zoo se compone de varios animales */
class Zoo
{
	// Esto es una relaci�n 1:1. Un zoo S�LO tiene un Animal(tigre)
	Animal* tigre;
	// Esto es una relaci�n 1:N. Un zoo tiene varios animales(pinguinos)
	Animal** pinguinos;

	int numPinguinos;
public:
	Zoo(int numPingunos);
	void mostrar();
	~Zoo();
};

