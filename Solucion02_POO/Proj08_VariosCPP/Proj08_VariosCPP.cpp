// Proj08_VariosCPP.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#include "OtraClaseDelPrograma.h"
#include "Zoo.h"

using namespace std;


int main()
{
	Animal* unSoloAnimal = new Animal(5);
	unSoloAnimal->mostrar();
	// unSoloAnimal[2].mostrar(); Es correcta para el compilador, pero no tiene sentido porque NO es un array de verdad
	// Este array se crea del tirón, todos los animales a la vez, el programa necesita que haya en la memoria un espacio libre seguido, contiguo, porque instancia todo el array a la vez.
	Animal* granja = new Animal[] {
		Animal(10),
		Animal(15),
		Animal(20)
	};
	granja->mostrar();	// Al revés sí porque mostramos el primer elemento
	granja[2].mostrar();

	cout << "PESO TOTAL = " << Animal::pesoTotal << "\n";
	cout << "PESO MIN = " << Animal::pesoMinimo << "\n";
	cout << "PESO MAX = " << Animal::pesoMaximo << "\n";

	OtraClaseDelPrograma otroObj;

	Zoo* zooJerez = new Zoo(5);
	if (zooJerez != NULL)
	zooJerez->mostrar();

	Zoo* zooSevilla = new Zoo(3);
	zooSevilla->mostrar();
	/*
	Animal** granja = new Animal*[] {
		new Animal(10),
			new Animal(15),
			new Animal(20)
	};*/
	delete unSoloAnimal;
	delete[] granja;
	delete zooJerez;
	delete zooSevilla;
}
// Ejercicio: 2 variables estáticas: pesoMinimo y pesoMaximo, para el animal menos pesado y para el más pesado. Añadir un 4º animal, y mostrar los valores