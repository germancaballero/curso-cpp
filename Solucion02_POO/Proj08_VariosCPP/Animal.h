#pragma once

#include <iostream>
using namespace std;

class Animal {
	int peso;

public:
	// Las variables en el fondo son variables globales pero vinculadas a una clase
	static int totalAnimales;
	static int pesoTotal;
	static int pesoMinimo;
	static int pesoMaximo;


	Animal(int peso) {	// En el momento en el que ponemos un constructos, 
		// C++ ya no a�ade un constructor por defecto vac�o
		cout << "Animal con " << peso << " Kg creado\n";
		this->peso = peso;
		pesoTotal += peso;
		pesoMinimo = pesoMinimo > peso ? peso : pesoMinimo;
		pesoMaximo = pesoMaximo < peso ? peso : pesoMaximo;
		Animal::totalAnimales++;
	}
	void mostrar() {
		cout << "Animal de " << peso << " Kg\n";
	}
	int getPeso() { return peso; }	// Aqu� peso se refiere a la propiedad porque no hay conflicto

	~Animal() {
		// Animal::totalAnimales--;
		cout << "Animal con " << peso << " Kg destruido. Quedan " 
			<< --Animal::totalAnimales << "\n";
	}
};
