#include "Zoo.h"

/* Estos animales creados se crean de uno en uno, cada uno con propio new, por lo tanto es muy din�mico, porque cada animal se reserva en un espacio independiente. Lo 'malo' es que hay que liberarlos de uno*/
Zoo::Zoo(int numPingunos) {
	tigre = new Animal(300);
	/*this->pinguinos = new Animal * [] {
			new Animal(3),
			new Animal(4),
			new Animal(5)
	};*/
	this->numPinguinos = numPingunos;
	this->pinguinos = new Animal * [numPingunos];
	for (int i = 0; i < numPingunos; i++) {
		this->pinguinos[i] = new Animal(3 + i);	// El peso es n� pinguino m�s 3
	}
}

void Zoo::mostrar() {
	cout << "ZOO: El tigre pesa " << tigre->getPeso() << "\n";
	cout << "     y tenemos " << this->numPinguinos << " pinguinos que pesan:\n";

	for (int i = 0; i < this->numPinguinos; i++) {
		cout << this->pinguinos[i]->getPeso() << ", ";
	}
	cout << "\n ";
}
// En los destructores conviene liberar la memoria de sus propios objetos o espacios reservados
Zoo::~Zoo() {
	
	delete tigre;
	for (int i = 0; i < this->numPinguinos; i++) {
		delete this->pinguinos[i];
	}
	delete[] this->pinguinos;
	cout << "Destruyendo Zoom. Se han quitado " << (numPinguinos + 1) << "animales\n";
}