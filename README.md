Programación Orientada a Objetos con C++
Duración:                     60 horas
Perfil:                            Básico
Dirigido a
Esta acción formativa va dirigida a Programadores que quieran adquirir el dominio
del lenguaje C++.
Requisitos previos 
Los alumnos necesitarán tener experiencia con algún lenguaje de programación
estructurado, así como los conceptos de orientación a objetos.  
Objetivos
Los alumnos entenderán los fundamentos del lenguaje de programación C++ y las
técnicas de programación orientada a objetos, creando una base sólida de
conceptos y conocimientos que luego utilizarán en su día a día profesional.

Contenido
1. Introducción al C++
2. Bases del lenguaje C++
1. Variables, operadores y tipos
2. Control de flujo
3. Arrays y punteros
1. Arrays
2. Swap
3. Arrays multidimensionales
4. Punteros
5. Memoria dinámica, new y delete
6. Variables de referencia
7. Acceso a los atributos y métodos
8. Puntero void
4. Programación Orientada a Objetos
1. Clases en C++

2. Visibilidad
3. El puntero this
4. Cabecera (.h) y código (.cpp) de una clase
5. Objetos const
6. Atributos y métodos static
7. Funciones y clases friend
5. Sobrecarga de operadores
6. Agregación, composición, inicializadores de constructor
7. Herencia
1. Introducción a la herencia
2. Herencia básica
3. Especificaciones de acceso (visibilidad)
4. Accediendo a miembros y cambiando su visibilidad
5. Herencia Múltiple
6. Clases bases virtuales
8. Funciones virtuales
1. Punteros a la clase padre y a la clase hija
2. Funciones virtuales
3. Clases abstractas y Funciones virtuales puras
4. Clases interface
9. Trabajando con ficheros
1. ofstream
2. ifstream
3. Ficheros de acceso aleatorio
4. fstream
10. Plantillas (templates)
1. Funciones con templates
2. Clases con templates
3. Especialización
11. Excepciones
1. try, catch y throw
2. catch all y especificadores de excepciones
3. Herencia y la clase Exception
12. Standard Template Library (STL)
1. Introducción
2. Colecciones
3. Iteradores
4. Algoritmos
