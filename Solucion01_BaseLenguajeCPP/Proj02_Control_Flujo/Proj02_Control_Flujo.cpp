// Proj02_Control_Flujo.cpp 
#include <iostream>

// Declarar es indicar que una variable o función existe y su estructura básica.
void ejemplosCondicionales();
void ejerciciosCondicionales();
void ejemplosBucles();
void ejerBucles();
void ejerBucles2();
void ejemploSwitch();


int main()
{
    // La llamada a la función
    // ejemplosCondicionales();
    // ejerciciosCondicionales();
    // ejemplosBucles();
    // ejerBucles2();
    // ejemploSwitch();
}

// Implementar, además de indicar la estructura, indicamos los parámetros y sobre todo, aquí está el cuerpo (el código interno) de la función
void ejemplosCondicionales() {
    std::cout << "CONDICIONALES: \n";
    // if  (condicion) instruccion;  TODO US UNA SÓLA INSTRUCCIÓN
    if (10 > 4) std::cout << "10 > 4 es cierto\n";
    // POR LEGIBILIDAD SE PUEDE PONER en varias linbeas
    if (1 > 4)
        std::cout << "1 > 4 es cierto\n";

    // Por otro lado tenemos los bloques, que para el compilador es cómo una sóla instrucción
    {
        int variable_bloque = 0;
        std::cout << "Cualquier cosa\n";
        std::cout << "Otra cosa\n" << variable_bloque;
    }
    // std::cout << "Fuera, no existe la variable, se ha destruido\n" << variable_bloque;
    if (10 > 4) {
        std::cout << "Bloque en condicional\n";
    }
    int num_usu;
    std::cout << "Intruduzca con respeto un numero: \n";
    std::cin >> num_usu;
    std::cout << "Has puesto " << num_usu << "\n";
    //Condicional compuesto:
    if (num_usu > 10) {
        std::cout << "Numero es mayour que 10" << "\n";
    }
    else {
        std::cout << "Numero NO es mayor que 10" << "\n";
    }
}

void ejerciciosCondicionales() {

    /* Ejercicio 1: Pide dos números al usuario,
    y dile cual es mayor de los dos o si son iguales */
    int n1, n2;

    std::cout << "N1? \n";
    std::cin >> n1;
    std::cout << "N2? \n";
    std::cin >> n2;
    if (n1 > n2)
    {
        std::cout << n1 << "es mayor que " << n2 << "\n";
    }
    else {
        if (n2 > n1)
        {
            std::cout << n2 << "es mayor que " << n1 << "\n";
        }
        else {
            std::cout << n2 << "es igual " << n1 << "\n";
        }
    }

    /* Ejercicio 1:
            Realizar un programa que pida cargar una fecha cualquiera,
            luego verificar si dicha fecha corresponde a Navidad.   */

    unsigned  short mes, dia;
    std::cout << "Ejercicio 1: Mes? ";
    std::cin >> mes;
    std::cout << "             Dia? ";
    std::cin >> dia;
    if (mes == 0 || mes > 12 || dia == 0 || dia > 31) {
        std::cout << "Fecha incorrecta ";
    }
    else {// Y si no, la fecha es correcta:
        if (mes == 12 && /* AND */ dia >= 24 || /* OR */ mes == 1 && dia <= 6) {
            std::cout << "\nEs Navidad. \n";
        }
        else {
            std::cout << "\nNO es Navidad. \n";
        }
    }

    /* Ejercicio 2:
        Se ingresan por teclado tres números, si todos los valores ingresados son menores a 10,
        imprimir en pantalla la leyenda "Todos los números son menores a diez".
            */
    int m1, m2, m3;

    std::cout << "Num 1? ";
    std::cin >> m1;
    std::cout << "Num 2? ";
    std::cin >> m2;
    std::cout << "Num 3? ";
    std::cin >> m3;
    if (m1 < 10 && m2 < 10 && m3 < 10) {
        std::cout << "Todos los números son menores a diez. ";
    }
    /*     Ej 3:
        Escribir un programa que pida ingresar la coordenada de un punto en el plano, es decir dos valores enteros x e y (distintos a cero).
           Posteriormente imprimir en pantalla en que cuadrante se ubica dicho punto. (1º Cuadrante si x > 0 Y y > 0 , 2º Cuadrante: x < 0 Y y > 0, etc.)
   */

    int x, y;

    std::cout << "\nNum X? ";
    std::cin >> x;
    std::cout << "\nNum Y? ";
    std::cin >> y;
    if (x > 0 && y > 0) {
        std::cout << "\nCuadrante 1";
    }
    else if (x < 0 && y > 0) {
        std::cout << "\nCuadrante 2";
    }
    else if (x < 0 && y < 0) {
        std::cout << "\nCuadrante 3";
    }
    else if (x > 0 && y < 0) {
        std::cout << "\nCuadrante 4";
    }
    else if (x == 0 && y != 0) {
        std::cout << "\nEje Y";
    }
    else if (y == 0 && x != 0) {
        std::cout << "\nEje X";
    }
    else { // if (x == 0 && y == 0) {
        std::cout << "\nCentro";
    }
    int comp = 0;
    if (comp) { // Falso, no lo muestra
        std::cout << "\nFalso";
    }
    if (100) {  // Verdadero
        std::cout << "\nVerdadero";
    }
}

void ejemplosBucles() {
    // Bucle padre de todos: WHILE:  while (cond) instruccion;
    // Se va a ejecutar mientras el valor numérico sea distinto de cero. Es decir, que la condición sea verdadera
    
    // EJ:  Mostrar los números pares hasta el 10
    int n = 0; 
    while ( n <= 10 ) {
        // % es el resto de la división entera.
        // Si un número / 2, el resto es 1, el número impar
        if (n % 2 == 0) {
            std::cout << "\n N = " << n;
        }
        n = n + 1;
    }
    n = 10;
    while (n <= 20) {
        std::cout << "\n N = " << n;
        n += 2;
    }
    //  El bucle FOR es un bucle WHILE, que si queremos, podemos tunear para recorrer variables de un  valor a otro
    // for (inicializacion; condicion; repeticion) instruccion;
    // n = 20;
    for (int m = 20; n <= 30; n += 2) {
        std::cout << "\n N = " << n;
        // n += 2;
        // La repetición se ejecuta AL FINAL del bucle siempre
    }
    // std::cout << "\n Fuera N vale " << m;
    // Bucle DO-WHILE, siempre al menos se ejecuta una vez
    // do instruccion; while (condicion);
    int num;
    do {
        std::cout << "\nIndica un numero";
        std::cin >> num;

    } while (num != 0);
}

// Ejercicios: en una nueva función:
void ejerBucles() {
    // Ejer 1: Se ingresan un conjunto de n alturas de personas por teclado. Mostrar la altura promedio de las personas. 

    int n = 0; //repeticiones
    char r;  //variable para seguir metiendo alturas o parar (en caso de ser 0)
    int s;  //altura asignada
    int ss = 0;   //suma de alturas
    do {
        n = n + 1;
        std::cout << "\n altura de la primera persona ";
        std::cin >> s;
        ss = ss + s;
        std::cout << "\n quieres seguir metiendo alturas? pulse 0 para parar";
        std::cin >> r;

    } while (r != 's' && r != 'S');
    std::cout << "\n numero de personas? " << n;
    std::cout << "\n media de alturas " << ss / n;


    // Ejer 2: Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado) 

      //n; numero a imprimir
      // r; repeticion
    for (int n = 11, r = 0; r < 25; n += 11, r += 1) {
        std::cout << "\n " << n;
    }

    for (int r = 0; r < 25; r++) {
        std::cout << "\n " << (r + 1) * 11;
    }
}
/*#define y &&*/
#define hacer {
#define fin }
#define procedimiento void
#define entero int
#define mientras while 

procedimiento ejerBucles2() hacer
    // Ejer 3: Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50) 
    entero i = 1;

    while (i <= 10   &&   true) {
        std::cout << i << " x 5 = " << (i * 5) << "\n";
        i++;
    }
    // Ejer 4: Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
    // Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante.Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.
    int c, x, y,  q1 = 0, q2 = 0, q3 = 0, q4 = 0;
    // Al terminar, usar un array en vez de q1, q2...

    std::cout << "\nCantidad puntos cuadrantes? ";

    for (std::cin >> c, i = 0; i < c; i++) {

        std::cout << "\nNum X? ";
        std::cin >> x;
        std::cout << "\nNum Y? ";
        std::cin >> y;
        if (x >= 0 && y >= 0) {
            std::cout << "\nCuadrante 1";
            q1++;
        }
        else if (x < 0 && y >= 0) {
            std::cout << "\nCuadrante 2";
            q2++;
        }
        else if (x < 0 && y < 0) {
            std::cout << "\nCuadrante 3";
            q3++;
        }
        else { // if (x >= 0 && y < 0) {
            std::cout << "\nCuadrante 4";
            q4++;
        }
    }
    std::cout << "\nCuadrante 1: " << q1;
    std::cout << "\nCuadrante 2: " << q2;
    std::cout << "\nCuadrante 3: " << q3;
    std::cout << "\nCuadrante 4: " << q4;
}

void ejemploSwitch() {
    int eleccion;
    do
    {
        std::cout << "\n Dime que num quieres";
        std::cin >> eleccion;
        switch (eleccion)
        {
        case 0: // if (eleccion == 0)
            std::cout << "\n CERO";
            std::cout << "\n CERO";
            std::cout << "\n SIN ALCOHOL";
            break;
        case 1:// else if (eleccion == 1)
            std::cout << "\n UNO";
            break;
        case 2:
            std::cout << "\n DOS";
            break;
        case 3:
            std::cout << "\n TRES";
            break;
        case 4:
        case 5:
        case 6: case 7:case 8:case 9:
            std::cout << "\n UNOS POCOS";
            break;
        case 10:
            std::cout << "\n DIEZ";
            break;
        default:    // else
            std::cout << "\n MUCHOS";
            break;
        }
    } while (eleccion >= 0);
}