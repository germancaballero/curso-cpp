// Proj01_Intro.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>

int main()
{
	std::cout << "Variables, operadores y tipos!\n";
	int numero, num2, num3;	// Declaración de variable: El compilidador reserva memoria
	numero = 200;	// El operador de asignación =
	num2 = 2;
	num3 = 3;

	// num3 no está inicializado, tiene basura
	numero = num2 + num3;
	// * para multiplicar y / dividir.
	// También % calula el módulo (el resto de la división entera)
	std::cout << numero << "\n";
	int division, resto_division, calculo;
	division = numero / num3;	// 5 / 3 = 1.66666 >>> 1
	resto_division = numero % num3;
	std::cout << "numero: " << numero << " y num2: " <<
		num2 << " y num3: " << num3 << "\n";
	std::cout << "Division: " << division << " y resto div: " <<
		resto_division << "\n";
	std::cout << "Numero: " << numero;
	calculo = (numero + num2) * num3;

	int multiplic = num2 * num3;
	std::cout << "\n\nMultiplic: " << multiplic << "\n";
	multiplic = multiplic * 2;
	std::cout << "Multiplic: " << multiplic << "\n";

	// Abreviatura para operar y asignar a la misma variable;
	multiplic *= num2;
	std::cout << "Multiplic: " << multiplic << "\n";

	// Comparaciones
	int v1 = 10, v2 = 20, v3 = 30;
	std::cout << "COMPARACIONES: valor1 =" << v1 
		<< "; valor2 = " << v2 << "\n";
	std::cout << "¿v1 == v2? " << (v1 == v2) << "\n";
	std::cout << "¿v1 != v2? " << (v1 != v2) << "\n";
	std::cout << "¿v1 < v2? " << (v1 < v2) << "\n";
	std::cout << "¿v1 >= v2? " << (v1 >= v2) << "\n";
	// Operador AND
	std::cout << "v1 < v2  Y v1 < v3?" << (v1 < v2 && v1 < v3) << "\n";
	std::cout << "v1 < v2  Y v1 < v3  Y v3 <= v2?" << (v1 < v2 && v1 < v3 && v3 <= v2) << "\n";
	// Operador OR
	std::cout << "v1 < v2  O v1 < v3?" << (v1 < v2 || v1 < v3) << "\n";
	std::cout << "v1 > v2  O v1 > v3  O v3 <= v2?" << (v1 > v2 || v1 > v3 || v3 <= v2) << "\n";
	
	// Operador ternario es una condición que devuelve un valor:
	std::cout << "v3 != v2?  " << ( v3 != v2 ? "VERDAD" : "FALSO" ) << "\n";
	// Si es verdadero, le damos un valor de 1000 y si no 2000
	int cantidad = v3 < v2 ? 1000 : 2000;
	std::cout << "cantidad =  " << cantidad << "\n";

	// TIPOS DE DATOS:
	char num_pequeno = 65;	// 1 byte de -128 a 127
	std::cout << "num_pequeno =  " << num_pequeno + 0 << "\n";
	std::cout << "num_pequeno =  " << num_pequeno << "\n";
	unsigned char char_ssigno = 300;// 1 byte de 0 a 255
	std::cout << "char_ssigno =  " << char_ssigno + 0 << "\n";
	std::cout << "char_ssigno =  " << char_ssigno << "\n";
	// short: 2 bytes -32.768 hasta 32.767
	// unsigned short: 2 bytes -> 0 hasta  65535
	unsigned short num_mediano = 66000;
	std::cout << "num_mediano =  " << num_mediano << "\n";
	// int: depende de la plataforma, pero por lo general, son 4 bytes (pero pueden ser 2 bytes en 32 bit):
	// desde -2.000.000.000  hasta +2.000.000.000
	// unsigned int desde 0 hasta +2.000.000.000
	int num_grande = 2000000000;
	std::cout << "num_grande =  " << num_grande << "\n";
	unsigned int num_grande_ss = 4000000000;
	std::cout << "num_grande =  " << num_grande_ss << "\n";
	// long por lo general son 4 bytes
	long num_supergrande = 5000000000;
	std::cout << "num_supergrande =  " << num_supergrande << "\n";
	long long num_supermegagrande = 5000000000444554;
	// 8 bytes: Un huevo
	std::cout << "num_supermegagrande =  " << num_supermegagrande << "\n";
	typedef long long INT64;
	INT64 mi_meganum = 3312432432432434;
	std::cout << "mi_meganum =  " << mi_meganum << "\n";

	// 123.545.554  1,2354554 * 10 ^ 8
	// 1,2354554 E 8
	// Números decimales:
	// float usa 4 bytes, 4 * 8 = 32 bits
	// 8 bits para el exponente +/-	2 ^	-128 a 127		
	// 24 bits para la mantisa
	// -16777216 a 16777215. 
	// El float tiene entre 7 y 8 decimales de PRECISION
	float f1 = 4.4545324667734345656;
	float f2 = 4444444444444444444.0;
	float f3 = 0.000005556565656565656;

	std::cout << "f1=" << f1 << ", f2=" << f2 << " f3=" << f3 << "\n";

	// double usa 8 bytes (como long long) por lo que tiene entre 15 y 16 decimales de precisión
	double d1 = 4.4545324667734345656;
	double d2 = 4444444444444444444.0;
	double d3 = 0.000005556565656565656;

	std::cout << "d1=" << d1 << ", d2=" << d2 << " d3=" << d3 << "\n";
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
