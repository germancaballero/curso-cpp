#include <iostream>

void funcion_ints(int num1, int num2) {	// parámteros es lo que recibe la función
	// Los parámetros son variables locales que reciben copias de los valores de los argumentos
	std::cout << "funcion_ints ( " << num1 << " , " << num2 << " ) \n";
	num1 = num1 * 3;
	num2 = num2 + 20;
	std::cout << "cambio funcion_ints ( " << num1 << " , " << num2 << " ) \n";

	int array[3] = { 33, 44, 55 };
	// Al terminar, las variables locales se destruyen y se libera esa memoria, incluidos los arrays estáticos.
}
void funcion_recibe_ptr(int* ptr_num1, int* ptr_num2) {
	int array[3] = { 33, 44, 55 };
	std::cout << "funcion_ints ( " << ptr_num1 << " , " << ptr_num2 << " ) \n";
	// Para mostrar el contenido, usamos el * delante
	std::cout << "funcion_ints ( " << *ptr_num1 << " , " << *ptr_num2 << " ) \n";
	*ptr_num1 = *ptr_num1 * 3;
	*ptr_num2 = *ptr_num2 + 20;
	std::cout << "cambio funcion_ints ( " << *ptr_num1 << " , " << *ptr_num2 << " ) \n";
}

void ejemplo_paso_por_valor() {
	int x = 10, y = 20;
	funcion_ints(x, y);	// argumentos es lo que le pasas a la función

	std::cout << "1 - Fuera de funcion_ints ( " << x << " , " << y << " ) \n";

	funcion_ints(66, 77);

	funcion_recibe_ptr(&x, &y);	// argumentos es lo que le pasas a la función
	std::cout << "2 - Fuera de funcion_ints ( " << x << " , " << y << " ) \n";
}