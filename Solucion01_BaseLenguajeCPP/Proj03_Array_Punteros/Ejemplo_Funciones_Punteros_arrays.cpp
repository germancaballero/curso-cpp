#include <iostream>

void funcion_array(int[], int);
void funcion_ptr_array(int* numeros, int tam);

void ejemplo_funciones_punteros_arrays() {
	int lista_1[] = { 1, 2, 3 };
	funcion_array(lista_1, 3);
	int* ptr_lista_1 = lista_1;	// Aqu� no copiamos el array entero, s�lo la direcc de memoria
	funcion_array(ptr_lista_1, 3);	// Un punero se puede pasar como argumento a un par�metro que sea array, porque un array es un puntero
	funcion_ptr_array(lista_1, 3);	// Lo que le pasamos es la direcci�n del array, por eso el array se convierte en puntero

	funcion_ptr_array(ptr_lista_1, 3);	// Lo que le pasamos es la direcci�n del array, por eso el array se convierte en puntero
}
// Realmente recibimos una direcci�n a lo que deber�a ser un array
void funcion_array(int numeros[], int tam) {
	std::cout << "Llamada funcion_array: \n";
	for (int i = 0; i < tam; i++) 
		std::cout << "numeros [ " << i << " ] = " << numeros[i] << " \n";

	std::cout << " & numeros = " << (int) numeros << " \n";
	for (int i = 0; i < tam; i++) {
		std::cout << "numeros [ " << i << " ] = & " << (int) &numeros[i] << " \n";
		std::cout << "numeros [ " << i << " ] = & " << (int) ( numeros + i ) << " \n";
		std::cout << "numeros [ " << i << " ] = * " << *(numeros + i) << " \n";
		*(numeros + i) = *(numeros + i) * 2;
	}
}
// Aunque esta funci�n recibe un puntero
void funcion_ptr_array(int* numeros, int tam) {
	std::cout << "Llamada funcion_ptr_array: \n";
	for (int i = 0; i < tam; i++) {
		// Aqu� lo usamos como un array, con []
		std::cout << "numeros [ " << i << " ] = " << numeros[i] << " \n";

		numeros[i] = numeros[i] * 2;
	}
}
// POR LO TANTO, LOS ARRAYS NO SE PUEDEN PASAR POR VALOR, SIEMPRE SE PASA SU DIRECCI�N DE MEMORIA, PORQUE UN ARRAY ES UN PUNTERO