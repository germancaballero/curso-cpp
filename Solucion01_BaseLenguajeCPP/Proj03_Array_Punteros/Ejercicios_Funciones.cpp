#include <iostream>
int pedir_numero();
void tabla_multiplicar(int);
int sumar(int, int);
int quadrante(int, int);
void mostrar_maximo(int, int num2);

// Ejercicio 1: Combinar las funciones pedir_numero y tabla_multiplicar, para pedir al usuario de qu� numero quiere mostrar la tabla de multiplicar

void ejercicio1() {
	/*int num = pedir_numero();
	tabla_multiplicar(num);*/

	tabla_multiplicar(pedir_numero());
}
// Ej 2: Pedir dos n�meros mediante esa funci�n, y sumarlos, mostrar el cuadrante y el m�ximo de los dos.

void ejercicio2() {
	int num1 = pedir_numero();
	int num2 = pedir_numero();
	std::cout << "Suma de " << num1 << " y " << num2 << " = " << sumar(num1, num2) << "\n";
	int q = quadrante(num1, num2);
	std::cout << "Cuadrante es = " << q << "\n" ;
	mostrar_maximo(num1, num2);
}
// Ej 3: Funci�n que reciba un array de enteros y devuelva la suma de todos sus valores
int sumar_array(int[], int );

void ejercicio3() {
	int mi_array[5] = { 1, 2, 3, 4, 5 };
	int total = sumar_array(mi_array, 5);
	std::cout << "TOTAL ARRAY = " << total << "\n";
}
int sumar_array(int* array_num, int tamano) {
	int suma = 0;
	for (int i = 0; i < tamano; i++) {
		suma += array_num[i];
	}
	return suma;
}

// Ej 4: Crear una funci�n que calcule el valor m�ximo de 3 n�meros.
int maximo(int x, int y, int z);
void ejercicio4() {
	std::cout << "Maximo: " << maximo(3, 5, 8) << "\n";
	std::cout << "Maximo: " << maximo(8, 3, 5) << "\n";
	std::cout << "Maximo: " << maximo(3, 8, 8) << "\n";
}
int maximo(int x, int y, int z) {

	if (x >= y && x >= z)
		return x;
	else if (y >= z)
		return y;
	else
		return z;
}
// Ej 5: Crear una funci�n que calcule el valor m�ximo de X n�meros(recibir� un array y la cantidad del array. Recordad que un array es un puntero y viceversa, as� que el par�metro ser� un puntero
int maximo_en_array(int *array_m, int tam) {
	int maximo = array_m[0];
	for (int i = 1; i < tam; i++) {
		if (array_m[i] > maximo) {
			maximo = array_m[i];
		}
	}
	return maximo;
}
void ejercicio5() {
	int mi_array[] = { 10, 2, 3, 4, 5, 3, 3 };
	int total = maximo_en_array(mi_array, 7);
	std::cout << "Maximo: " << total << "\n";
}
// Ej 6: Crea una funci�n que calcule la media (el valor medio � media aritm�tica) de 3 num�ros. Pero recibiendo punteros. Que devuelva un float
float media_3(short* x, short* y, short* z) {
	
	float sum = *x + *y + *z;
	float media = sum / 3.0f;
	return media;
}
void ejercicio6() {
	short n1 = 10000, n2 = 14, n3 = 16;
	std::cout << "Media 3: " << media_3(&n1, &n2, &n3);
	short x = 200;
	int y = x;
	y = 10000;
	short z = y;
	std::cout << "X: " << z;
	long long w = 2;
	short  o = w;
	double doble = 3.343434;
	float f = doble;
	// std::cout << "Media 3: " << media_3(4, 5, 9);
}
// Ej 7: Crea una funci�n que calcule la media (el valor medio � media aritm�tica) de X num�ros. (recibir� un array y la cantidad del array)

double media_en_array(int array_m[], int tam) {
	double sum = 0;
	for (int i = 0; i < tam; i++) {
		sum += array_m[i];
	}
	return sum / (double) tam;
}
void ejercicio7() {
	int mi_array[] = { 1234567891, 1234567891, 1234567891, 1234567891 };
	double media = media_en_array(mi_array, 4);
	std::cout << "Media 2: " << media << "\n";

	int mi_array2[] = { 2, 3, 8, 9 };
	media = media_en_array(mi_array2, 4);
	std::cout << "Media 2: " << media << "\n";
}
// Ej 8: La misma pero recibir� dos punteros(array y tama�o y lo haremos con aritm�tica de punteros.
int media_en_array_ptr(int* array_m, int *tam) {
	double sum = 0;
	for (int i = 0; i < *tam; i++) {
		// sum += array_m[i];
		sum += *(array_m + i);
	}
	return sum / (double) *tam;
}
void ejercicio8() {
	int mi_array2[] = { 2, 3, 8, 9 };
	int t = 4;
	double media = media_en_array_ptr(mi_array2, &t);
	std::cout << "Media 2: " << media << "\n";
}


