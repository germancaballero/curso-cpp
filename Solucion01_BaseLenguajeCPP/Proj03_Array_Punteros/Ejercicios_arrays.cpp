#include <iostream>

void ejerBuclesArray() {

	// Ejer 4: Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
	// Informar cu�ntos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante.Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.
	int i, c, q[4] = { 0, 0, 0, 0 }, x, y; // q[0] = 0; q1[1]; q1[2]
	// Al terminar, usar un array en vez de q1, q2...

	std::cout << "\Cantidad puntos cuadrantes? ";

	for (std::cin >> c, i = 0; i < c; i++) {

		std::cout << "\nNum X? ";
		std::cin >> x;
		std::cout << "\nNum Y? ";
		std::cin >> y;
		if (x >= 0 && y >= 0) {
			std::cout << "\nCuadrante 1";
			q[0]++;
		}
		else if (x < 0 && y >= 0) {
			std::cout << "\nCuadrante 2";
			q[1]++;
		}
		else if (x < 0 && y < 0) {
			std::cout << "\nCuadrante 3";
			q[2]++;
		}
		else { // if (x >= 0 && y < 0) {
			std::cout << "\nCuadrante 4";
			q[3]++;
		}
		std::cout << "\nX = " << x << ", y = " << y;

	}
	for (i = 0; i < c; i++) {
		std::cout << "\nCuadrante " << i << ": " << q[i];
	}
}

void ejerBuclesArray2() {

	// Ejer 4: Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
	// Informar cu�ntos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante.Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.
	int i, c, q[4] = { 0, 0, 0, 0 }, x, y; // q[0] = 0; q1[1]; q1[2]
	// Al terminar, usar un array en vez de q1, q2...

	std::cout << "\Cantidad puntos cuadrantes? ";

	for (std::cin >> c, i = 0; i < c; i++) {
		std::cout << "\nNum X? ";
		std::cin >> x;
		std::cout << "\nNum Y? ";
		std::cin >> y;
		q[0] += (x >= 0 && y >= 0) ? 1 : 0;
		q[1] += (x < 0 && y >= 0) ? 1 : 0;
		q[2] += (x < 0 && y < 0) ? 1 : 0;
		q[3] += (x >= 0 && y < 0) ? 1 : 0;

		std::cout << "\nX = " << x << ", y = " << y;
	}
	for (i = 0; i < c; i++) {
		std::cout << "\nCuadrante " << i << ": " << q[i];
	}
}
