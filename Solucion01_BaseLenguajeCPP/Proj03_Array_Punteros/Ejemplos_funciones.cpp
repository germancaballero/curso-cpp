// Una funci�n es un trozo de c�digo, que recibe datos, hace cosas y devuelve un �nico dato (aunque ser compuesto, o un array)
#include <iostream>

void tabla_multiplicar(int num) {

    int i = 1;

    if (num < 1 || num > 10) {
        std::cout << " Numero muy peque�o o grande " << "\n";
    }
    while (i <= 10 && true) {
        std::cout << i << " x " << num << " = " << (i * num) << "\n";
        i++;
    }
}
// Varios par�metros:
void mostrar_maximo(int n1, int n2) {
    if (n1 > n2) {
        std::cout << n1 << " es el m�ximo\n";
    }
    else if (n1 < n2) {
        std::cout << n2 << " es el m�ximo\n";
    }
    else {
        std::cout << n2 << " con iguales\n";
    }
}
// Sin par�metros, pero que devuelve algo
// devuelve un �nico dato(aunque ser compuesto (structs, clases), o un array)
int pedir_numero(void) {
    int num;
    std::cout << "Escribe un numero: ";
    std::cin >> num;
    return num;
}
// Por supuesto, puede recibir valores y devolver valores
int sumar(int x, int y) {
    return x + y;
}
// Y por dentro, todoi lo complejas que queramos
int quadrante(int x, int y) {

    if (x >= 0 && y >= 0) {
        return 1;
    }
    else if (x < 0 && y >= 0) {
        return 2;
    }
    else if (x < 0 && y < 0) {
        return 3;
    }
    else { // if (x >= 0 && y < 0) {
        return 4;
    }
}