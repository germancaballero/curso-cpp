#include <iostream>

void ejemploArray() {

	std::cout << "Array:\n";
	// Un array es un tipo de dato para variables, donde se almacenan una COLECCI�N de elementos, de tama�o FIJO, todas del MISMO TIPO
	// Tambi�n es un espacio de memoria FIJO (RAM) reservado por el compilador para poder almacenar una colecci�n de...
	int notas_clase[5]; // 4 * 5 = 20 bytes
	// En C el primer elemento es el 0
	notas_clase[0] = 7;
	notas_clase[1] = 3;
	notas_clase[2] = 7;
	notas_clase[3] = 5;
	notas_clase[4] = 7; // El �ltimo es la long-1
	for (int i = 0; i < 5; i++) {
		std::cout << "notas_clase[" << i << "] = " << notas_clase[i] << "\n";
	}
}