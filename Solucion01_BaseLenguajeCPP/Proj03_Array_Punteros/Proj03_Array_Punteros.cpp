#include <iostream>

void ejerBuclesArray();
void ejerBuclesArray2();
void ejemploArray();
void que_es_un_puntero();
void un_array_es_un_puntero();
void arrays_caracteres();
// Nos importa la firma (estructura) de la funci�n
void tabla_multiplicar(int);
void mostrar_maximo(int, int num2);
int pedir_numero();
int sumar(int, int);
int quadrante(int, int);
void ejercicio1();
void ejercicio2();
void ejercicio3();
void ejercicio4();
void ejercicio5();
void ejercicio6();
void ejercicio7();
void ejercicio8();

void ejemplo_paso_por_valor();
void ejemplo_funciones_punteros_arrays();

int main()
{
	//ejerBuclesArray2(); 
	//ejemploArray();
	// que_es_un_puntero();
	// un_array_es_un_puntero();
	// arrays_caracteres();
	/*tabla_multiplicar(3);
	tabla_multiplicar(6);
	tabla_multiplicar(9);
	mostrar_maximo(20, 30);
	mostrar_maximo(20, 10);
	mostrar_maximo(10, 10);*/
	// Cuando una funci�n devuelve algo, podemos recoger el valor:
	/*int el_numero = pedir_numero();
	std::cout << " Has escrito " << el_numero << "\n";
	std::cout << " Otro numero " << pedir_numero() << "\n";
	std::cout << sumar(10, 15) << "\n";
	std::cout << quadrante(10, 15) << "\n";
	std::cout << quadrante(-10, -15) << "\n";
	ejercicio1();
	ejercicio2();
	ejercicio5();
	*/
	ejemplo_paso_por_valor();
	/*ejercicio7();
	ejercicio8();*/
	//ejemplo_funciones_punteros_arrays();
}