#include <iostream>

void que_es_un_puntero() {

	// Todo en C se guarda en direcciones de memoria (datos y c�digo binario)
	// Un puntero es una variable que apunta a una direcci�n de memoria
	// Un puntero puede apuntar a cualquier cosa (variable, constante, array, funci�n...)
	// �Qu� tipo de dato es en el fondo? Tiene que ser entero (long int)

	float num = 1.2345;
	// Declaraci�n ptr, asterisco despu�s del tipo de dato:
	float* ptr_num;
	// �Como sabemos la direcci�n de una variable?  Con &
	ptr_num = &num;
	long long valor_ptr_num = (int) ptr_num;	// Sabemos que se puede, pero hay que casting expl�cito

	std::cout << "Variable: " << num << ", direcc. mem: " << ptr_num << ", valor direcc. mem: " << valor_ptr_num << "\n";
	std::cout << "Tamano var: " << sizeof(num) << ", Tamano ptr: " << sizeof(ptr_num) << "\n";
	// Para mostrar el contenido, pido disculpas de antemano, se usa tambi�n el '*'
	std::cout << "Contenido ptr: " << *ptr_num << "\n";


	double numd = 1.2345;
	double* ptr_numd;
	ptr_numd = &numd;
	long long valor_ptr_numd = (int)ptr_numd;
	std::cout << "Variable: " << numd << ", direcc. mem: " << ptr_numd << ", valor direcc. mem: " << valor_ptr_numd << "\n";
	std::cout << "Tamano var: " << sizeof(numd) << ", Tamano ptr: " << sizeof(ptr_numd) << "\n";
	std::cout << "Contenido ptr: " << *ptr_numd << "\n";

	char numc = 'J';
	char* ptr_numc;
	ptr_numc = &numc;
	long long valor_ptr_numc = (int)ptr_numc;
	std::cout << "Variable: " << numc << ", direcc. mem: " << ptr_numc << ", valor direcc. mem: " << valor_ptr_numc << "\n";
	std::cout << "Tamano var: " << sizeof(numc) << ", Tamano ptr: " << sizeof(ptr_numc) << "\n";
	std::cout << "Contenido ptr: " << *ptr_numc << "\n";

}
void un_array_es_un_puntero() {
	short numeritos[3];
	for (int i = 0; i < 3; i++)		numeritos[i] = i * 9 + 9;

	// Usamos & para saber la direcci�n de memoria de algo
	std::cout << "Direcc mem numeritos[0] = " << (int) &numeritos[0] << "\n";
	std::cout << "Direcc mem numeritos =    " << (int)numeritos << "\n";
	// Mostramos contenido elementos array:
	std::cout << "numeritos[0]           =  " << numeritos[0] << "\n";
	std::cout << "Contenido numeritos    =  " << * numeritos << "\n";
	std::cout << "Contenido numeritos[0] =  " << * ( & numeritos[0]) << "\n";
}

void arrays_caracteres() {
	// Los textos base en C (y en C++, aunque ya veremos que hay clases como string que nos ayudar�n)
	char texto[10];
	char* ptr_texto;
	// string copy, copia de cadena de texto. Recibe 2 punteros, fuente y destino, y el tama�o del destino
	strcpy_s(texto, 10, "Que pasa!");
	std::cout << "Texto: " << texto;

	ptr_texto = texto;
	std::cout << "\nTexto: " << ptr_texto;

	// �Entonces, un puntero se puede usar como si fuera un array?	SI

	std::cout << "\n Pos 5 : " << ptr_texto[5];
}