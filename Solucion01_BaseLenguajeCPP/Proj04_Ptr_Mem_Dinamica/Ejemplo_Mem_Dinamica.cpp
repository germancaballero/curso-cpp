// Include realmente es un copia y pega del fichero
#include "FicheroCabecezon.h"

void ejemplo_casting() {
	const float PI = 3.141592535f;
	int PI_entero =  PI;	// Conversi�n impl�cita
	PI_entero = (int)PI;	// Conversi�n expl�cita
	std::cout << "PI entero = " << PI_entero << "\n";
}

void ejemplo_reserva_mem() {
	std::cout << "Ejemplo reserva memoria\n";
	int num_entero; // Se resenvan 4 bytes 
	int* ptr_entero; // Se reservan 4 bytes en 32 bit, 8 en 64 bit

	// Con malloc reservamos una cantidad de bytes
	// malloc = memory allocate
	ptr_entero = (int *) malloc(4); // Cuidado, mejor sizeof(int)
	*ptr_entero = 20;
	num_entero = 30;
	cout << "Suma contenido ptr y variable y constante: "
		<< (*ptr_entero + num_entero + 40);
	// LO QUE NUNCA SE NOS PUEDE OLVIDAR:
	// LIBERAR MEMORIAR
	free(ptr_entero);
}

int ejemplo_array_dinamico()
{
    int* pe;    // En el fondo, ser� un array din�mico
    int tam;
    int f;
    cout << "\nCuantos elementos tendra el vector:";
    cin >> tam;
    pe = (int *) malloc(tam * sizeof(int));
    for (f = 0; f < tam; f++)
    {
        cout << "\nIngrese elemento:";
        cin >> pe[f]; // *(pe + f);
    }
    cout << ("Contenido del vector dinamico:");
    for (f = 0; f < tam; f++)
    {
        cout << *( pe + f)   << "\n";
    }
    free(pe);
    return 0;
}

void ejemplo_texto_array_char() {
    char texto_array[] = "Hola, que \0pasa!";
    cout << texto_array << "\n";
    cout << "Tam: " << sizeof(texto_array) << "\n";
    cout << "a = " << texto_array[3] << "m " << (short) texto_array[3] << "\n";

    char texto_2[6];
    texto_2[0] = 'H';
    texto_2[1] = 'O';
    texto_2[2] = 'L';
    texto_2[3] = 65;
    texto_2[4] = 70; // lo mismo que '\0'
    cout << "Hola?:  " << texto_2 << "\n";

    char texto_3[] = { 65, 'D', 'I', 'O', 'S', '\0' };
    cout << "Adios?:  " << texto_3 << "\n";
}