#include "FicheroCabecezon.h"

// Ejercicio: El mismo ejemplo ejemplo_texto_array_char, pero con memoria din�mica
void ejercicio_texto_array_char() {
    char texto_array[] = "Hola, que \0pasa!";
    int tam_array = sizeof(texto_array);
    char* ptr_array = (char*) malloc(tam_array);
    for (int i = 0; i < tam_array; i++) {
        ptr_array[i] = texto_array[i];
    }
    cout << texto_array << "\n";
    cout << "Tam: " << sizeof(texto_array) << "\n";
    cout << "a = " << texto_array[3] << "m " << (short)texto_array[3] << "\n";
    free(ptr_array);

    const char *texto_array_const = "Hola, que \0pasa!";
    /* 
    char* texto_array;
    int t = sizeof("Hola, que pasa!");
    texto_array = (char*)malloc(t);
    strcpy_s(texto_array, t, "Hola, que \0pasa!");
    cout << texto_array << "\n";
    cout << "Tam: " << sizeof(texto_array) << "\n";
    cout << "a = " << texto_array[3] << "m " << (short)texto_array[3] << "\n";*/
    char texto_2[6];
    texto_2[0] = 'H';
    texto_2[1] = 'O';
    texto_2[2] = 'L';
    texto_2[3] = 65;
    texto_2[4] = 70; // lo mismo que '\0'
    cout << "Hola?:  " << texto_2 << "\n";

    char texto_3[] = { 65, 'D', 'I', 'O', 'S', '\0' };
    cout << "Adios?:  " << texto_3 << "\n";

    // Crear un nuevo array din�mico, y copiamos los valores de texto_3, o bien n�mero a n�mero (char a char) o bien con strcpy()
}