// #pragma once
// En los ficheros de cabecera se suelen usar para otros includes, definiciones (#define), declaraciones de funciones

#include <iostream>
#include <string>

using namespace std;

void ejemplo_casting();
void ejemplo_reserva_mem();
int ejemplo_array_dinamico();
void ejemplo_texto_array_char();
void ejercicio_texto_array_char();