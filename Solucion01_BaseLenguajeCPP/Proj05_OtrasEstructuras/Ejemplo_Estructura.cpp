#include "Cabecera.h"



void ejemplo_estructura_1() {
	st_aperitivo mis_alitas;
	// El punto, es un operador de acceso por excelencia
	mis_alitas.cantidad = 3;
	strcpy_s(mis_alitas.nombre, 50, "Alitas de pollo");
	mis_alitas.precio = 0.95;
	cout << "Tu aperitivo " << mis_alitas.nombre << " te habr�n costado " << (mis_alitas.cantidad * mis_alitas.precio) << "\n";

	st_aperitivo tus_3_ape[3];
	tus_3_ape[0].cantidad = 5;
	strcpy_s(tus_3_ape[0].nombre, 50, "Berenjenas");
	tus_3_ape[0].precio = 1.95;
	tus_3_ape[1].cantidad = 8;
	strcpy_s(tus_3_ape[1].nombre, 50, "Bravas");
	tus_3_ape[1].precio = 3.95;
	tus_3_ape[2].cantidad = 1;
	strcpy_s(tus_3_ape[2].nombre, 50, "Orejas");
	tus_3_ape[2].precio = 9.95;

	st_lista_aperitivos tu_lista_ape;
	tu_lista_ape.tam = 3;
	tu_lista_ape.aperitivos = tus_3_ape;
	mostrar_aperitivos(tu_lista_ape);
}

void mostrar_aperitivos(st_lista_aperitivos lista_ape) {

	for (int i = 0; i < lista_ape.tam; i++) {
		cout << lista_ape.aperitivos[i].nombre << ": "
			 << lista_ape.aperitivos[i].cantidad << "\n";
	}
}
void ejemplo_array_dinamico_estructura() {
	// El operador de acceso a propiedades de un puntero a estructura es    ->

	st_lista_aperitivos* aper_bar;
	aper_bar = (st_lista_aperitivos*)malloc(sizeof(st_lista_aperitivos));
	
	cout << "\n�Cuantos aperitivos hay hoy?\n";
	cin >> (aper_bar->tam);
	aper_bar->aperitivos = (st_aperitivo*)malloc(aper_bar->tam * sizeof(st_aperitivo));
	for (int i = 0; i < aper_bar->tam; i++) {
		cout << "\nNombre aperitivo: ";
		char n[50];
		//getchar();
		cin >> n;
		int tam_nombre = strlen(n) * sizeof(char) + 1;
		// aper_bar->aperitivos[i].nombre = (char*) malloc(tam_nombre);
		strcpy_s(aper_bar->aperitivos[i].nombre, 50, n);

		cout << "\nCantidad: ";
		//getchar();
		cin >> aper_bar->aperitivos[i].cantidad;
	}
	mostrar_aperitivos(*aper_bar); // Aunque sea un puntero, como la funci�n recibe una estructura normal, se pasa por valor, se crea una copia.
}