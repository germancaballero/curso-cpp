#pragma once

#include <iostream>
#include <string>
//#include "string.h>

using namespace std;

typedef  long long entero;
// Cuando necesitamos combinar diferentes tipos de datos en uno s�lo, necesitaremos crear nuestra propia estructura:
typedef struct {
	int cantidad;		// variable miembro
	char nombre[50];	// Tambi�n llamados atributos, campos, propiedades...
	float precio;
}   st_aperitivo;

typedef struct {
	int tam;
	st_aperitivo* aperitivos;
} st_lista_aperitivos;

void ejemplo_array_bidim();
void ejemplo_estructura_1();
void mostrar_aperitivos(st_lista_aperitivos lista_ape);
void ejemplo_array_dinamico_estructura();