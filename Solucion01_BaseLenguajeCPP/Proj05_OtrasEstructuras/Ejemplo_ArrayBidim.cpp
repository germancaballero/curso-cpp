#include "Cabecera.h"

void mostrar_bidim(char array_bidim[][8]) {

	cout << "\n";
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			cout << array_bidim[i][j];
		}
		cout << "\n";
	}
}

void ejemplo_array_bidim() {
	cout << "Arrays de 2 dimensiones:\n";
	char tablero_ajedrez[8][8];
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			tablero_ajedrez[i][j] = ((i + j ) % 2 == 0 ) ? 'N' : '.';
		}
	}
	mostrar_bidim(tablero_ajedrez);
	// Ejercicio, pedir al usuario una posicion del tablero, y si la posici�n es correcta (dentro del tablero), pedir la pieza (un caracter) y colocar en el tablero. Volver a mostrar en el tablero.
	int x, y;	char pieza;
	cout << "\nIndica la fila (1 a 8): ";		cin >> x;
	cout << "\nIndica la columna (1 a 8): ";	cin >> y;
	if (x >= 1 && x <= 8 && y >= 1 && y <= 8) {
		cout << "\nPieza (Una letra): ";			cin >> pieza;
		cout << "\nSe coloca en " << x << ", " << y << " el " << pieza;
		tablero_ajedrez[x - 1][y - 1] = pieza;
		mostrar_bidim(tablero_ajedrez);
		exit(0);
	}
	else {
		cout << "\nNO entra en el tablero\n";
		exit(-1);
	}
}